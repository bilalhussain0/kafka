package controller;

import Client.ClientIRIS;
import consumer.MessageConsumer;
import model.MessageData;
import producer.MessageProducer;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.sql.SQLException;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AtmController {

    @Inject
    MessageProducer producer;
    String message;


    @GET
    @Path("/home/{count}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("count") int count)
    {
        ClientIRIS client=new ClientIRIS();
        client.main(count);

        return Response.ok("Message sent to server").build();
    }
    @GET
    @Path("/home2/{count}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response gettime(@PathParam("count") int count) throws SQLException {
        for(int i=0;i<count;i++){
            MessageData messageData=new MessageData();
            messageData.msg="PHX8583012000000          202103171855480200923499000660        6200000000000500000000000000000000000000000317185548        000000006143011855480317    0318        0003      021    63939000000000001                                                                                                                                                  614301210317      5692349900                       Islamabad                Islamabad    PK56                                                                                                   0406                                        586   586                                                                                                                                                                                        01050095007140019           01050095007140019                                         01050095007140019                                 6393900000062719700000C";
            producer.sendMsgToKafka(messageData);
        }

        return Response.ok("Message sent to server").build();
    }

}