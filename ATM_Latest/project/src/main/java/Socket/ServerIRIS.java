package Socket;


import database.data;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import model.MessageData;
import model.Process;
import producer.MessageProducer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.crypto.Data;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;


@QuarkusMain
@ApplicationScoped
public class ServerIRIS implements QuarkusApplication {

    @Inject
    MessageProducer producer;

    @Inject
    data da;

    byte detain[];
    static String   msg ;
    static Socket s;
    static BufferedOutputStream bout;
    static  BufferedInputStream bin;


    @Override
    public int run(String... args) throws Exception {
        main();

        Quarkus.waitForExit();
        return 0;
    }
    public void main() {
        try {

            da.start();
            ServerProcess ser=new ServerProcess();
            ser.start();
            ServerResponse responseServer=new ServerResponse();
            responseServer.start();
            ServerSocket ss = new ServerSocket(1111);
            System.out.println("server is listening");
            s = ss.accept();//establishes connection


            while (true){

                bin= new BufferedInputStream(s.getInputStream());
                byte[] msg_len = new byte[2];
                int len = (bin.read(msg_len, 0, 2));

                if(len>0){

                    int start = (int) System.currentTimeMillis();
                    System.out.println("Start server : " +start);
                    int msgSize;
                    msgSize = 256 * (msg_len[0] < 0 ? msg_len[0] + 256 : msg_len[0]);
                    msgSize += msg_len[1] < 0 ? msg_len[1] + 256 : msg_len[1];
                    detain = new byte[msgSize];

                    int counter = 0;

                    while (counter < msgSize) {

                        int d = bin.read();
                        detain[counter] = (byte) d;
                        counter++;
                    }

                    msg = new String(detain);

                    MessageData message = new MessageData();
                    message.msg=msg;
                    message.message_time= start;
                    System.out.println("Start server : " +message.message_time);
                    producer.sendMsgToKafka(message);


                }
                continue;
            }


        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void sent(String msg1,int num) throws IOException, SQLException {
        bout=new BufferedOutputStream(s.getOutputStream());

        byte[]  bit=Encode(msg1);
        byte[] msg=msg1.getBytes();


        byte[] combined=new byte[bit.length+msg.length];

        for(int i=0; i<combined.length; i++)
        {
            combined[i]=i<bit.length ? bit[i]: msg[i-bit.length];
        }

        bout.write(combined);

        bout.flush();
        System.out.println("Complete message Process:   "+num);



    }
    public byte[] Encode(String msg){

        int len2 = msg.length();

        byte[] sendLen = new byte[2];
        sendLen[0] = (byte) (len2 / 256);
        sendLen[1] = (byte) (len2 % 256);


        return sendLen;
    }



}