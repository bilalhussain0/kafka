package Socket;

import model.MessageData;
import producer.MessageProducer;

import javax.enterprise.context.ApplicationScoped;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerResponse extends Thread{
    static Socket s;
    static BufferedOutputStream bout;

    byte[] detain;
    byte[] msg_len;
    String     msg;
    public void run() {
        try {
            ServerSocket ss=new ServerSocket(1212);
            Socket s=ss.accept();

            int len=0;
            msg_len=new byte[2];
            while (true){

                BufferedInputStream   bin= new BufferedInputStream(s.getInputStream());
                byte[] msg_len = new byte[2];
                len = (bin.read(msg_len, 0, 2));
                if(len>0){
                    int msgSize;
                    msgSize = 256 * (msg_len[0] < 0 ? msg_len[0] + 256 : msg_len[0]);
                    msgSize += msg_len[1] < 0 ? msg_len[1] + 256 : msg_len[1];
                    detain = new byte[msgSize];
                    int counter = 0;
				    while (counter < msgSize) {

                        int d = bin.read();
                        detain[counter] = (byte) d;
                        counter++;
                    }

                    msg = new String(detain);

                    MessageData messageData = new MessageData();
                    messageData.msg=msg;

                    MessageProducer producer = new MessageProducer();
                    producer.sendResponseMsgToKafka(messageData);

                    continue;
                }
            }
        }catch (Exception e) {
            // TODO: handle exception
        }
    }




}
