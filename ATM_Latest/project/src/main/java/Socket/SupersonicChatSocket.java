package Socket;

import consumer.MessageConsumer;
import database.data;
import io.quarkus.scheduler.Scheduled;
import model.Process;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;



@ServerEndpoint("/chat/{username}")
@ApplicationScoped
public class SupersonicChatSocket {
    @Inject
    data d;
   static int count=0;
    private static final Logger LOG = Logger.getLogger(SupersonicChatSocket.class);

    Map<String, Session> sessions = new ConcurrentHashMap<>();


    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        System.out.println("open connection");
        sessions.put(username, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("username") String username) {
        System.out.println("close connection");
        sessions.remove(username);

    }

    @OnError
    public void onError(Session session, @PathParam("username") String username, Throwable throwable) {
        sessions.remove(username);
        System.out.println("error connection");
        LOG.error("onError", throwable);

    }

    @OnMessage
    public void onMessage(String message, @PathParam("username") String username) {
        System.out.println("message connection");

        if (sessions != null) {


        }
    }

    //Graph Componenets
    int counter=0;
    int completedValueMn;
    int completedValueHr;
    int lastCompletedValueMn = 0;
    int lastCompletedValueHr = 0;
    int iterationValue=0; //Works for time
    //Graph Componenets


    @Scheduled(every="3s")
    void increment() throws SQLException {
        if (sessions != null) {

            MessageConsumer consumer=new MessageConsumer();
            long requestSize=consumer.consumer("ATM","app");
            long responseSize=consumer.consumer("responseatm","app1");
            long reqPending=requestSize-responseSize;
            Process process=new Process();
            process=d.completeProcess();
            long complete=responseSize-(long) process.completeprocess;
            long Inprocess=requestSize-(long) process.completeprocess;

            String jsontest=  "{\"MessageSize\":"+reqPending +", \"InProcess\":"+Inprocess+", \"CompleteTime\":"+process.atm_completedtime +", \"AverageTime\":"+process.averge +", \"ResponseSize\":"+complete +", \"CompleteProcess\":"+process.completeprocess+"}";

//            broadcast(jsontest);


            if(counter==2) // Timer for Graph Scheduler*counter
            {
                Formatter timeGet = new Formatter();

                // Creating a calendar
                Calendar gfg_calender = Calendar.getInstance();

                // Displaying hour using Format clas using  format
                // specifiers
                // '%tl' for hours and '%tM' for minutes
                timeGet = new Formatter();
                timeGet.format("%tl:%tM", gfg_calender,
                        gfg_calender);
                // Printing the current hour and minute
                System.out.println(timeGet);

                 iterationValue=  iterationValue+counter;
                completedValueMn=  process.completeprocess-lastCompletedValueMn ;

                lastCompletedValueMn=process.completeprocess;

                 String jsontest2=  "{\"MessageSize\":"+reqPending +", \"InProcess\":"+Inprocess+", \"CompleteTime\":"+process.atm_completedtime +", \"AverageTime\":"+process.averge +", \"ResponseSize\":"+complete +", \"CompleteProcess\":"+process.completeprocess+", \"curr_time\":"+ "\"" + timeGet + "\"" +", \"completed_value\":"+completedValueMn +"}";

                 System.out.println("Current Time Value : "+ timeGet);
                 System.out.println("Completed Value : "+ completedValueMn);


//////////////3*12///////////////////////////////
                 if(iterationValue%12==0){

                     completedValueHr=  process.completeprocess-lastCompletedValueHr ;

                     String jsontest3= "{\"curr_time_24\":"+ "\"" + timeGet + "\"" +", \"completed_value_24\":"+completedValueHr +", \"MessageSize\":"+reqPending +", \"InProcess\":"+Inprocess+", \"CompleteTime\":"+process.atm_completedtime +", \"AverageTime\":"+process.averge +", \"ResponseSize\":"+complete +", \"CompleteProcess\":"+process.completeprocess+", \"curr_time\":"+ "\"" + timeGet + "\"" +", \"completed_value\":"+completedValueMn +"}";

                  //   System.out.println("Current Time Value : "+ timeGet);
                 //    System.out.println("Completed Value : "+ completedValue);

                     lastCompletedValueHr=process.completeprocess;

                     broadcast(jsontest3);
                 }
                 else
                     broadcast(jsontest2);


                 counter=0;
            }
            else
                broadcast(jsontest);
            counter++; //Iteration for Minutes
        }
    }
    private void broadcast(String message2) {
        sessions.values().forEach(s -> {
            System.out.println("broadcast connection");
            s.getAsyncRemote().sendObject(message2, result -> {
                System.out.println("broadcast connection"+message2);
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }


}
