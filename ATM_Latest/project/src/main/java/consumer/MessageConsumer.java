package consumer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;


import Socket.ServerIRIS;
import Socket.ServerProcess;
import database.data;
import model.Process;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import model.MessageData;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;


@ApplicationScoped
public class MessageConsumer {
	@Inject
	data da;
	static long requestOffset=0;
	static int 	ResponseOffset;
	public String message;
	Process process=new Process();

	long currentOffsetProducer;


	@Incoming("atm")
	public void receive(ConsumerRecord<String, String> record) {
		try {

			ObjectMapper objectMapper = new ObjectMapper();
			MessageData messageData = objectMapper.readValue(record.value(), MessageData.class);
			requestOffset= (int) (record.offset()+1);
			ServerProcess ss=new ServerProcess();
			ss.sent(messageData.msg);
			System.out.println("Request Que Size.....................:::"+record.offset());
		} catch (Exception e) {
		}
	}
	@Incoming("atmResponse")
	public void receiveResponse(ConsumerRecord<String, String> record) {
		try {

			ObjectMapper objectMapper = new ObjectMapper();
			MessageData messageData1 = objectMapper.readValue(record.value(), MessageData.class);
			ResponseOffset= (int) (record.offset()+1);

			ServerIRIS server=new ServerIRIS();
			server.sent(messageData1.msg,ResponseOffset);


			int start1=da.StartTime(ResponseOffset);
			System.out.println("Start time : " + start1);
			int end = (int) System.currentTimeMillis();
			System.out.println("End  : " + end);
			float time =(end-start1)/1000f;
			System.out.println("time  : " + time);
			process.atm_message_status=1;
			process.atm_completedtime=time;
			da.updatePro(process,ResponseOffset);

		} catch (Exception e) {
		}

	}

//////////////////////////Getting last offset of kafka topic//////////////////////////////////////
	public  long consumer(String topic,String group){

			Properties properties=new Properties();
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"kafka:9092");
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,   StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG,group);
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");

		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);



		TopicPartition topicPartition = new TopicPartition(topic, 0);

		consumer.assign(Collections.singletonList(topicPartition));
		consumer.poll(Duration.ofMillis(100));
		consumer.seekToEnd(Collections.singletonList(topicPartition));
		currentOffsetProducer = consumer.position(topicPartition) ;

		long producersize=currentOffsetProducer;
		System.out.println(" Current Offset of Producer:" + currentOffsetProducer);


		return producersize;

	}
}
