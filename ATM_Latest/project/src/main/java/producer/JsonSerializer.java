package producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JsonSerializer<T> implements Serializer<T> {
    private final ObjectMapper objectMapper=new ObjectMapper();

    public JsonSerializer() {
    }
    public void configure(Map<String,?> config, boolean isKey)
    {
    }
    public byte[] serialize(String topic,T data)
    {
        if(data==null)
        {
            return null;
        }
        try{
            return objectMapper.writeValueAsBytes(data);
        }catch (Exception e){
            throw new SerializationException("Error in serialize",e);
        }
    }
    public void close(){}
}
