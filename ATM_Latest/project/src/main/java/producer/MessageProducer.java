package producer;

import io.agroal.api.AgroalDataSource;
import model.MessageData;
import model.Process;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;



@ApplicationScoped
public class MessageProducer
{
    @Inject
    AgroalDataSource bankAlHabibDataSource;

    Connection conn;
    static int count;

    public void sendMsgToKafka(MessageData msgData) throws SQLException
    {

        Properties props=new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"kafka:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,JsonSerializer.class);
        KafkaProducer<String,MessageData> producer=new KafkaProducer<String, MessageData>(props);
        ProducerRecord<String,MessageData> record=new ProducerRecord<String,MessageData>("ATM","1",msgData);
        producer.send(record);
        producer.close();
        int start = (int) System.currentTimeMillis();

        System.out.println("Start  ..................: " + start);
        Process process = new Process();
        process.atm_message=msgData.msg;
        process.message_start=start;
        process.atm_message_status=0;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime now = LocalDateTime.now();
        process.atm_messagedate= dtf.format(now);
        push_sms_log(process);

    }

    public void sendResponseMsgToKafka(MessageData msgData) throws InterruptedException {
        Properties props=new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"kafka:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,JsonSerializer.class);
        KafkaProducer<String,MessageData> producer=new KafkaProducer<String, MessageData>(props);
        ProducerRecord<String,MessageData> record=new ProducerRecord<String,MessageData>("responseatm","1",msgData);
        producer.send(record);
        producer.close();

    }

    public void push_sms_log(Process process) throws SQLException {

        try {
            try {
                conn = bankAlHabibDataSource.getConnection();
            } catch (Exception e) {
                System.out.println("Error in connection insert"  +e.getMessage());
            }
        }catch (Exception e){
            System.out.println("Error in connection"+ e.getMessage());
        }

        try {
            Statement statement = conn.createStatement();
            int Query_Data = statement.executeUpdate("INSERT INTO atmlog(atm_message, atm_completedtime,atm_message_status,atm_starttime,atm_count,atm_messagedate)" +
                    "VALUES('" + process.atm_message + "','" + process.atm_completedtime + "','" + process.atm_message_status + "','" + process.message_start + "','" + process.message_count + "','" + process.atm_messagedate + "')");
            System.out.println("Record inserted:"+ Query_Data);

            conn.close();
        }
        catch (Exception e)
        {
            System.out.println("Error in = "+ e);
        }
    }

}
