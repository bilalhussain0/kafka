package database;


import io.agroal.api.AgroalDataSource;
import model.Process;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ApplicationScoped
public class data extends Thread{

    @Inject
    AgroalDataSource bankAlHabibDataSource;
    static Connection conn=null;

    int numSelect;
    int messageSize,completeProcess;
    float Average;
    float completedtime;
    public void run() {
        try {
            try {
                conn  = bankAlHabibDataSource.getConnection();
            } catch (Exception e) {
                System.out.println("Error in connection" + e);
            }
        }catch (Exception e){
            System.out.println("Error in connection" + e);
        }
    }

    ///////////////////////update///////////////////////////
    public void updatePro(Process pro,int num) {
        numSelect=num;
        try {
            run();
            Statement statement = conn.createStatement();
            int Query_Data=statement.executeUpdate("update atmlog set atm_completedtime='"+pro.atm_completedtime+"',atm_message_status='"+pro.atm_message_status+"' where atm_id="+num);
            System.out.println("updated:"+Query_Data);

            conn.close();
        }
        catch (Exception e)
        {
            System.out.println("Error in ="+e);
        }
    }



    /////////////////count complete/////////////////////////
    public Process completeProcess() throws SQLException {


        Process process=new Process();
        try{
            run();
            Statement statement = conn.createStatement();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();

            String currentSystemTime= dtf.format(now);

            ResultSet rs=statement.executeQuery("select count(atm_message) as msg, sum(atm_message_status) as status_count ,avg(atm_completedtime) as avg from atmlog where atm_message_status = "+ 1 +" AND atm_messagedate='"+ currentSystemTime + "'" );
            while (rs.next()){
                messageSize=rs.getInt("msg");
                completeProcess=rs.getInt("status_count");
                Average= rs.getFloat("avg");

            }
//            ResultSet rs1=statement.executeQuery("select avg(atm_completedtime) as avg from atmlog where atm_message_status = "+ 1 +" AND atm_messagedate='"+ currentSystemTime + "'" );
//            while (rs1.next()){
//
//                Average= rs1.getFloat("avg");
//
//            }



            process.completeprocess=completeProcess;
            process.averge=Average;

            ResultSet rss=statement.executeQuery("select atm_completedtime as cmptime from atmlog where atm_id="+numSelect );

            while (rss.next()){
                completedtime =rss.getFloat("cmptime");

            }


            process.atm_completedtime=completedtime;

            conn.close();
            return process;
        }
        catch (Exception e)
        {
            System.out.println("Error in ="+e);
        }
        return process;
    }
    public int StartTime(int num1) throws SQLException {

        int start= 0;
        try {
            run();

            Statement statement = conn.createStatement();
            ResultSet rs=statement.executeQuery("select atm_starttime as time from atmlog where atm_id="+num1);

            while (rs.next()){
                start=  rs.getInt("time");

            }

            System.out.println("database Get time start:"+start);
            conn.close();
            return start;
        }
        catch (Exception e)
        {
            System.out.println("Error in start time ="+e);
        }
        return start;
    }


}
